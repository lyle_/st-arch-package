# st - simple terminal

`st` is the simple terminal emulator from [suckless.org](https://st.suckless.org).
The general approach is to take a simple codebase, configure and patch it to
your liking, and run that customized binary on your system.

This package incorporates my personal settings:

* Default font is `monospace`, my [fontconfig](https://gitlab.com/lyle_/dotfiles/blob/master/fontconfig/.config/fontconfig/fonts.conf)
  settings alias this to whatever I prefer at the moment
* Add alpha transparency for the background
* Hide the X cursor whenever a key is pressed and show it back when the mouse
  is moved in the terminal window
* Open contents of the clipboard using `xdg-open`
* Zoom in/out with Ctrl+Shift+k/j or u/d for larger intervals.


## Installing

    makepkg --install	# if you're re-installing, use --force
