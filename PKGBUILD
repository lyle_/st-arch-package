pkgname="st"
pkgver=0.8.1
pkgrel=1
pkgdesc="My personal package of the suckless.org terminal emulator"
arch=("x86_64")
url=https://st.suckless.org/
source=("https://dl.suckless.org/$pkgname/$pkgname-$pkgver.tar.gz"
    "https://st.suckless.org/patches/alpha/st-alpha-$pkgver.diff"
	'default-font.diff'
	'st-hidecursor-0.8.diff'
	'st-openclipboard-20180525-2c2500c.diff'
	'zoom.diff'
	)
md5sums=('92135aecdba29300bb2e274a55f5b71e'
         '267b426c0e69aab81adbba2ce968dbcf'
         'a0c50452f0b6f553a6afad49b97a1d6c'
         '8749ced53fc11d7a979fa8fa21d66de4'
         'f030f9c3c7217ee7cba3be8fc06bb0e0'
         '13c91160f99ddd9f431656b216146029')

prepare() {
	cd "$srcdir/$pkgname-$pkgver"

	# Add alpha transparency to the background
	patch -Np1 -i "${srcdir}/st-alpha-$pkgver.diff"

	# Apply hidecursor patch
	patch -Np1 -i "${srcdir}/st-hidecursor-0.8.diff"

	# Set background to black
	sed --in-place 's/defaultbg = 257;/defaultbg = 258;/' "${srcdir}/st-$pkgver/config.def.h"

	# Open URL in clipboard
	patch -Np1 -i "${srcdir}/st-openclipboard-20180525-2c2500c.diff"

	# Set default font
	patch -Np2 -i "${srcdir}/default-font.diff"

	# Zoom in/out with Ctrl+Shift+k/j or u/d for larger intervals
	patch -Np1 -i "${srcdir}/zoom.diff"
}

build() {
	cd "$pkgname-$pkgver"
	make clean
	make
}

package() {
	cd "$pkgname-$pkgver"
	make DESTDIR="$pkgdir/" install
}
